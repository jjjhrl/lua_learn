function producer()
    return coroutine.create(
        function(salt)
            coroutine.yield(salt)
            --[[
            local t={1, 2, 3}
            for i=1, #t do
                function add(a1, a2)
                    return a1+a2
                end
                salt = coroutine.yield(add(t[i], salt))
            end
            ]]
        end
    )
end

function consumer(prod)
    local salt = 10
    while true do
        local running, product = coroutine.resume(prod, salt)
        salt = salt * salt
        if running then
            print(product or "END!")
        else
            break
        end
    end
end

consumer(producer())
