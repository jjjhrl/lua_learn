--[[Lua的表既是Hash表，也是数组，可以把数组看作键为数值的Hash表]]

a = {}

b = {n=1, str='abc', 100, 'hello'}

a.n = 1
a.str = 'abc'
a[1] = 100
a[2] = 'hello'
a["a table"] = b

function func1() end
function func2() end

a[func1] = func2

for k, v in pairs(a) do
    print(k, "=>", v)
end
