--[[
--Table
haoel = {name="chenhao", age=37, handsome=true}

--Table's CRUD
haoel.website="http://coolshell.cn/"
local age = haoel.age
--haoel.handsome = false
haoel.name = nil

print("haoel.website:", haoel.website)
print("haoel.age:", age)
print("haoel.handsome:", haoel.handsome)
print("haoel.name:", haoel.name)

t = {[20]=100, ['name']="chenhao", [3.14]='PI'}
print(t[20])
print(t["name"])
print(t[3.14])
]]

-- arr = {10, 20, 30, 40, 560}
-- arr = {[1]=10, [2]=20, [3]=30, [4]=40, [5]=560}
arr = {"string", 100, "haoel", function() print("coolshell.cn") end}
print("arr:", arr)
print("arr[4]():")
arr[4]()

--[[
for i=1, #arr do
    print(arr[i])
end

print(_G.globalVar)
_G["globalVar"]="ahldjlajdl"
print(_G.globalVar)
]]

for k, v in pairs(arr) do
    print(k, v)
end
