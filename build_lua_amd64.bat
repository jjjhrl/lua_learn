call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" amd64

set "vc_dir=C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\amd64"

cd src
if exist .\src\*.obj del .\src\*.obj
if exist .\src\*.lib del .\src\*.lib
if exist .\src\*.exp del .\src\*.exp

"%vc_dir%\cl" /O2 /W3 /c /DLUA_BUILD_AS_DLL l*.c
del lua.obj luac.obj
"%vc_dir%\link" /DLL /out:lua.dll l*.obj

"%vc_dir%\cl" /O2 /W3 /c /DLUA_BUILD_AS_DLL lua.c luac.c
"%vc_dir%\link" /out:lua.exe lua.obj lua.lib
del lua.obj
"%vc_dir%\link" /out:luac.exe l*.obj
cd ..

set "ext=dest_lua_amd64"

if exist "%~dp0\%ext%" (
  rd /s/q "%~dp0\%ext%"
)
mkdir "%~dp0\%ext%"

move .\src\lua.exe "%~dp0\%ext%"
move .\src\luac.exe "%~dp0\%ext%"
move .\src\lua.dll "%~dp0\%ext%"

del .\src\*.obj
del .\src\*.lib
del .\src\*.exp

pause