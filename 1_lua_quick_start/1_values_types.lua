--[[
there are eight basic types in Lua:
nil, boolean, number, string, function, userdata, thread,table

a userdata value represents a block of raw memory.
]]

print(type(nil))
print(type(false))
print(type(30))
print(type("|sldjl"))
