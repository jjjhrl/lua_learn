--function

function fib(n)
    if n < 2 then return 1 end
    return fib(n -2) + fib(n - 1)
end

print("fib(10):", fib(10))

--closure
function newCounter()
    local i = 0
    return function()    --anonymous function
        i = i+1
        return i
    end
end

c1 = newCounter()
print(c1())
print(c1())

print(newCounter()())
print(newCounter()())

function myPower(x)
    return function(y) return y^x end
end

power2 = myPower(2)
power3 = myPower(3)

print(power2(4))
print(power3(5))

name, age, bGay = "haha", 17, false, "lajldj@lask.com"

function getUserInfo(id)
    print("id:", id)
    return "haha", 123, "lajdl", "http://aldkla.com0"
end

name, age, email, whebsite, bGay = getUserInfo()
