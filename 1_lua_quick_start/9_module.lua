--hello.lua
--定义名为hello的模块
--并使全局变量在此模块中可见
module('hello', package.seeall)

ver = "version 0.1"

function hello()
    print("hello!")
end

--test_hello.lua:
--使用模块
require "hello"

print(hello.ver)
hello.hello()
