
--[[

--while

sum = 0
num = 1
while num <= 100 do
  sum = sum + num
  num = num + 1
end
print("sum =", sum)

--if else
age = 40
sex = 'male'
if age == 40 and sex == 'Male' then
  print('male')
elseif age > 60 and sex ~= "Female" then
  print("old man")
elseif age < 20 then
  io.write('too young, too naive!\n')
else
  local age = io.read()
  print('your age is:'..age)
end

--for
sum = 0
for i = 1, 100 do
    sum = sum + i
end

sum = 0
for i = 100, 1, -2 do
    sum = sum + i
end

]]

sum = 2
repeat
    sum = sum ^ 2
    print(sum)
until sum > 1000
