t = {}
t2 = {a="and", b="Li lei", c="Han Meimei"}
m = {__index=t2}
setmetatable(t, m)    --设表m为表t的元表

for k, v in pairs(t) do
    print(k, v)
end
print("-------------")
print(t.b, t.a, t.c)

function add(t1, t2)
    local length = #t1
    for i=1, length do
        t1[i] = t1[i] + t2[i]
    end
    return t1
end

t1 = {1, 2, 3}
t3 = {10, 20, 30}

setmetatable(t1, {__add = add})
setmetatable(t3, {__add = add})

t1 = t1 + t3

for i=1, #t1 do
    print(t1[i])
end
