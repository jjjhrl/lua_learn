function foo()
    print(g or "'g' is not defined!")
end

foo()

env = {g = 100, print = print}
setfenv(foo, env)    --设置foo的环境为表env(lua5.1升5.2时已经把setfenv去掉)

foo()
print(g or "'g' is not defined!")
