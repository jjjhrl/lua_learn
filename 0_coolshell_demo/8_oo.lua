Person = {}

function Person:new(p)
    local obj = p
    if(obj == nil) then
        obj = {name="chenhao", age=37, handsome=false}
    end
    self.__index = self
    return setmetatable(obj, self)
end

function Person:toString()
    return self.name .. " : " .. self.age .. " : " .. (self.handsome and "handsome" or "ugly")
end

me = Person:new()
print(me:toString())

kf = Person:new({name="king's fucking", age=70, handsome=true})
print(kf:toString())

--[[
Strudent = Person:new()

function Student:new()
    newObj = {year=2013}
    self.__index = self
    return setmetatable(newObj, self)
end

function Student:toString()
    return "Student : " .. self.year .. " : " .. self.name
end
]]
