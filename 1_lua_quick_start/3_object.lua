function createFoo(name)
    local obj={name=name}
--[[
    function obj:SetName(name)
        self.name=name
    end

    function obj.SetName(self, name)
        self.name = name
    end
    ]]
    obj.SetName = function(self, name) self.name = name end
    --[[
    function obj:GetName()
        return self.name
    end
    ]]
    function obj.GetName()
        return obj.name
    end
    return obj
end

o = createFoo("sam")
print("name:",o.GetName())

o.SetName(o, "Lucy")
print("name:",o.GetName())
